from agent import AlphaBetaAgent
import minimax
import constants
import state_tools_basic
import time
import math
from constants import *

class TimeoutException(Exception):   # Custom exception class
    pass

"""
Agent skeleton. Fill in the gaps.
"""

class MyAgent(AlphaBetaAgent):

    values = {}

    @staticmethod
    def R(playerId,otherPlayerId,state):
        rocks1 = state_tools_basic.rocks(state,playerId)
        rocks2 = state_tools_basic.rocks(state,otherPlayerId)
        sum1 = 0
        sum2 = 0
        tmp = 0
        for ((y, x), f) in rocks1:
            if f == UP:
                tmp = 4 - y
            elif f == DOWN:
                tmp = y
            elif f == LEFT:
                tmp = 4 - x
            elif f == RIGHT:
                tmp = x
            if MyAgent.checkCounter(x,y,f,state,playerId) == otherPlayerId:
                sum2 += tmp
            else:
                sum1 += tmp

        for ((y, x), f) in rocks2:
            if f == UP:
                tmp = 4 - y
            elif f == DOWN:
                tmp = y
            elif f == LEFT:
                tmp = 4 - x
            elif f == RIGHT:
                tmp = x
            if MyAgent.checkCounter(x,y,f,state,otherPlayerId) == playerId:
                sum1 += tmp
            else:
                sum2 += tmp

        return sum1 - sum2

    @staticmethod
    def get_rocks(state):
        string = state.compact_str()
        i = 0
        j = 0
        rocks = []
        for char in string:
            if i >= 5:
                i = 0
                j = 1
            if char == '#':
                rocks.append((i,j))
            i += 1
        return rocks

    @staticmethod
    def alignedPlayer(playerID,otherPlayerID,state):
        player0 = state.get_player_piece_positions(playerID)
        player1 = state.get_player_piece_positions(otherPlayerID)
        rocks = MyAgent.get_rocks(state)
        sum1 = 0
        sum2 = 0
        for (y,x) in player0:
            for(x2,y2) in rocks:
                if x2 == x or y2 == y :
                    sum1 += 0.2
                    break
        for (y,x) in player1:
            for(x2,y2) in rocks:
                if x2 == x or y2 == y :
                    sum2 += 0.2
                    break
        return sum1 - sum2

    @staticmethod
    def reserve(playerID,otherPlayerID,state):
        return math.sqrt(state.reserve[0]) - math.sqrt(state.reserve[1])

    @staticmethod
    def checkCounter(x,y,f,state,defaultID):
        if f == RIGHT or f == LEFT:
            tmp = 0
            if f == RIGHT:
                tmp = range(x+1,5)
            if f == LEFT:
                tmp = reversed(range(0,x))
            if y == 0:
                for i in tmp:
                    if state.face[y+1][i] == UP:
                        return state.board[y+1][i]
            elif y == 4:
                for i in tmp:
                    if state.face[y-1][i] == DOWN:
                        return state.board[y-1][i]
            else:
                for i in tmp:
                    if (state.face[y + 1][i] == UP) != (state.face[y - 1][i] == DOWN):
                        if state.face[y - 1][i] == DOWN:
                            return state.board[y - 1][i]
                        else:
                            return state.board[y + 1][i]
        if f == UP or f == DOWN:
            tmp = 0
            if f == DOWN:
                tmp = range(y+1,5)
            if f == UP:
                tmp = reversed(range(0,y))
            if x == 0:
                for i in tmp:
                    if state.face[i][x+1] == LEFT:
                        return state.board[i][x+1]
            elif x == 4:
                for i in tmp:
                    if state.face[i][x-1] == RIGHT:
                        return state.board[i][x-1]
            else:
                for i in tmp:
                    if (state.face[i][x+1] == LEFT) != (state.face[i][x-1] == RIGHT):
                        if state.face[i][x+1] == LEFT:
                            return state.board[i][x+1]
                        else:
                            return state.board[i][x-1]
        return defaultID

    @staticmethod
    def get_distance(playerId,state):
        rocks = MyAgent.get_rocks(state)
        pos = state.get_player_piece_positions(playerId)
        sumDistance = 0
        leastDistance = math.inf
        for (x1,y1) in pos:
            for (x2,y2) in rocks:
                dist = math.hypot(x2 - x1,y2 - y1)
                if dist < leastDistance:
                    leastDistance = dist
            sumDistance += leastDistance
            leastDistance = math.inf
        return sumDistance



    iterDeep = 1
    startTime = 0

    """This is the skeleton of an agent to play the game."""
    
    def get_action(self, state,last_action,time_left):
        """This function is used to play a move according
        to the board, player and time left provided as input.
        It must return an action representing the move the player
        will perform.
        """
        global iterDeep
        global startTime
        startTime = time.time()
        iterDeep = 1
        action = None
        while True:
            try:
                print(iterDeep)
                action = minimax.search(state, self)
                iterDeep += 1
            except TimeoutException:
                print("No time left")
                return action

    def successors(self, state):
        """The successors function must return (or yield) a list of
        pairs (a, s) in which a is the action played to reach the
        state s;
        """
        #TODO: Ignorer des actions inutiles
        actions =  state.get_player_actions(self.id)
        actions2 = []
        for action in actions:
            newState = state.copy()
            newState.apply_action(action)
            actions2.append((action,newState))
        return actions2



    def cutoff(self, state, depth):
        """The cutoff function returns true if the alpha-beta/minimax
        search has to stop; false otherwise.
        """
        if time.time() - startTime > 5:
            raise TimeoutException("Plus assez de temps")
        if state.game_over() or depth == iterDeep:
            return True
        return False

    def evaluate(self, state):
        """The evaluate function must return an integer value
        representing the utility function of the board.
        """
        
        otherID = 0
        if(self.id == otherID):
            otherID = 1
        if(self.id == state.winner):
            return 100000
        elif(otherID == state.winner):
            return -100000
        oui = 2*MyAgent.R(self.id,otherID,state) + MyAgent.alignedPlayer(self.id,otherID,state) + MyAgent.reserve(self.id,otherID,state)
        return oui




